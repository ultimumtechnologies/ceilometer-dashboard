PANEL_DASHBOARD = "project"
PANEL_GROUP = "default"
PANEL = "ceilometer"

ADD_PANEL = "ceilometer_dashboard.dashboards.project.ceilometer.panel.CeilometerPanel"

ADD_INSTALLED_APPS = ['ceilometer_dashboard']
AUTO_DISCOVER_STATIC_FILES = True

ADD_ANGULAR_MODULES = ['horizon.dashboard.project.ceilometer-dashboard']

#ADD_SCSS_FILES = ['dashboard/project/grafana/grafana.scss']

# Automatically discover static resources in installed apps


# A list of js files to be included in the compressed set of files
#ADD_JS_FILES = []


# A list of template-based views to be added to the header
# ADD_HEADER_SECTIONS = ['myplugin.content.mypanel.views.HeaderView',]
