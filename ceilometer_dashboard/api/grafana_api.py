import json
import requests
from requests.utils import requote_uri
from pprint import pprint


class GrafanaException(Exception):
    pass


class GrafanaServerError(Exception):
    pass


class GrafanaUnauthorizedError(Exception):
    pass


class GrafanaPermissionError(Exception):
    pass


class GrafanaNotFoundError(Exception):
    pass


class GrafanaObjExistsError(Exception):
    pass


class GrafanaAPI:
    default_headers = {'X-WEBAUTH-USER': 'admin', 'Content-Type': 'application/json', 'Accept': 'application/json'}

    def __init__(self, url=None, headers=None, throw_exceptions=True):
        if url is None:
            url = 'http://localhost:3000/api'
        self.url = url
        if headers is None:
            headers = GrafanaAPI.default_headers
        self.headers = headers
        self.throw_exceptions = throw_exceptions
        self.sess = requests.Session()
        self.sess.headers.update(self.headers)

    def __getattr__(self, item):
        def __action(url, json=None):
            __url = '{0}/{1}'.format(self.url, url)
            action = getattr(self.sess, item)
            resp = action(__url, json=json, headers=self.headers)

            if self.throw_exceptions:
                if 500 <= resp.status_code < 600:
                    raise GrafanaServerError("Server Error {0}: {1}".format(resp.status_code,
                                                                            resp.content.decode("ascii", "replace")))
                elif resp.status_code == 401:
                    raise GrafanaUnauthorizedError('Error {0}: {1}'.format(resp.status_code, resp.text))
                elif resp.status_code == 403:
                    raise GrafanaPermissionError('Error {0}: {1}'.format(resp.status_code, resp.text))
                elif resp.status_code == 404:
                    raise GrafanaNotFoundError('Error {0}: {1}'.format(resp.status_code, resp.text))
                elif resp.status_code == 409:
                    raise GrafanaObjExistsError('Error {0}: {1}'.format(resp.status_code, resp.text))
                elif resp.status_code != 200:
                    raise GrafanaException('Error {0}: {1}'.format(resp.status_code, resp.text))

                return resp.json()

            else:
                return resp

        return __action


class GrafanaManage:
    def __init__(self, url=None, headers=None):
        if url is None:
            url = 'http://localhost:3000/api'
        if headers is None:
            headers = GrafanaAPI.default_headers
        self.api = GrafanaAPI(url, headers)
        self.org_id = None

    def get_org_id(self, name):
        try:
            org = self.api.get('orgs/name/{}'.format(name))
            return org.get('id')
        except GrafanaNotFoundError:
            return None

    def get_team_id(self, name):
        try:
            teams = self.api.get('teams/search?name={}'.format(name))
            if teams['teams']:
                try:
                    return [team for team in teams['teams'] if team['name'] == name][0].get('id')
                except IndexError:
                    raise GrafanaException('Could not get teamId. Team "{}" exists and is unassigned. Please, delete it from database.'.format(name))
        except GrafanaNotFoundError:
            return None

    def get_user_id(self, name):
        try:
            user = self.api.get('users/lookup?loginOrEmail={}'.format(name))
            return user.get('id')
        except GrafanaNotFoundError:
            return None

    def get_datasource_id(self, name):
        try:
            org = self.api.get('datasources/id/{}'.format(name))
            return org.get('id')
        except GrafanaNotFoundError:
            return None

    def is_user_in_org(self, login, org_id):
        try:
            members = self.api.get('orgs/{}/users'.format(org_id))
            user = [member for member in members if member['login'] == login]
            if user:
                return user[0]
        except GrafanaNotFoundError:
            return None

    def is_user_in_team(self, login, team_id):
        try:
            members = self.api.get('teams/{}/members'.format(team_id))
            user = [member for member in members if member['login'] == login]
            if user:
                return user[0]
        except GrafanaNotFoundError:
            return None

    def create_org(self, name):
        try:
            org = self.api.post('orgs', json={'name': name})
            return org.get('orgId')
        except GrafanaObjExistsError:
            return None

    def switch_context(self, org_id):
        try:
            return self.api.post('user/using/{}'.format(org_id))
        except GrafanaUnauthorizedError:
            return None

    def create_team(self, org_id, name):
        if self.switch_context(org_id):
            try:
                team = self.api.post('teams', json={'name': name})
                return team.get('teamId')
            except GrafanaObjExistsError:
                return None

    def create_global_user(self, login, name, password, email=None):
        if email is None:
            email = ''
        json = {'login': login, 'password': password, 'email': email, 'name': name}
        try:
            user = self.api.post('admin/users', json=json)
            return user.get('id')
        except GrafanaObjExistsError:
            return None

    def add_team_member(self, teamid, userid):
        try:
            return self.api.post('teams/{}/members'.format(teamid), json={'userId': userid})
        except:
            return None

    def assign_user_org_role(self, org_id, login_or_email, role='Viewer'):
        try:
            return self.api.post('orgs/{}/users'.format(org_id), json={'loginOrEmail': login_or_email, 'role': role})
        except GrafanaObjExistsError:
            return None

    def create_grafana_os_user(self, login, name, password, org, email=None):
        team = org
        org_id = self.get_org_id(org)
        if not org_id:
            org_id = self.create_org(org)
        pprint('orgId: {}'.format(org_id))

        if org_id:
            team_id = self.get_team_id(team)
            if not team_id:
                team_id = self.create_team(org_id, team)
            pprint('teamId: {}'.format(team_id))
            if team_id:
                user_id = self.get_user_id(login) or self.create_global_user(login, name, password, email=email)
                pprint('user id: {}'.format(user_id))
                if user_id:
                    user_in_team = self.is_user_in_team(login, team_id)
                    if user_in_team:
                        user_team_id = user_in_team.get('teamId')
                    else:
                        user_team_id = self.add_team_member(team_id, user_id)
                    pprint('user team: {}'.format(user_team_id))

                    user_in_org = self.is_user_in_org(login, org_id)
                    if user_in_org:
                        user_org_id = user_in_org.get('orgId')
                    else:
                        user_org_id = self.assign_user_org_role(org_id, login, role='Editor')
                    pprint('user role: {}'.format(user_org_id))
                    if user_team_id and user_org_id:
                        return user_id

    def create_gnocchi_keystone_data_source(self, name, os_ks_url, os_domain, os_project, os_user, os_password):
        try:
            ds_json = {'name': name,
                       'type': 'gnocchixyz-gnocchi-datasource',
                       'access': 'proxy',
                       'url': os_ks_url ,
                       'jsonData': {'mode':'keystone',
                                   'domain': os_domain,
                                   'password': os_password,
                                   'project': os_project,
                                   'username': os_user}
                       }

            dsource = self.api.post('datasources', json=ds_json)
            return dsource.get('id')
        except GrafanaObjExistsError:
            return None

    def create_grafana_os_structure(self, user_login, user_name, password, org, keystone_url, email=None, domain='default'):
        dsource_name = org_name = '{0}.{1}'.format(domain, org)

        g_user_id = self.create_grafana_os_user(user_login, user_name, password, org_name, email=email)
        if g_user_id:
            dsource_id = self.get_datasource_id(dsource_name)
            if not dsource_id:
                dsource_id = self.create_gnocchi_keystone_data_source(dsource_name, keystone_url, domain, org, user_login, password)
            pprint('data source: {}'.format(dsource_id))
            return dsource_id

    def check_grafana_os_structure(self, user_login, org, domain='default'):
        results = []
        dsource_name = org_name = '{0}.{1}'.format(domain, org)

        org_id = self.get_org_id(org_name)
        results.append(self.get_user_id(user_login))
        results.append(self.is_user_in_org(user_login, org_id))

        team_id = self.get_team_id(org_name)
        results.append(self.is_user_in_team(user_login, team_id))
        results.append(self.get_datasource_id(dsource_name))
        return all(results)