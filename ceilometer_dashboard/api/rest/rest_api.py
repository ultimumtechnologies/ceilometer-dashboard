from django.views import generic
from openstack_dashboard.api.rest import urls
from openstack_dashboard.api.rest import utils as rest_utils

from ceilometer_dashboard.api.grafana_api import GrafanaManage as GrafanaManager

from django.conf import settings

try:
    GRAFANA_API = settings.GRAFANA_API
except AttributeError:
    GRAFANA_API = 'http://localhost:3000/api'


@urls.register
class GrafanaUsers(generic.View):
    url_regex = r'ceilometer/users/$'
    api_url = GRAFANA_API

    @rest_utils.ajax()
    def get(self, request):
        grafana_manager = GrafanaManager(self.api_url)

        user_id = grafana_manager.get_user_id(request.user.id)
        organization_id = grafana_manager.get_org_id(request.user.project_id)
        team_id = grafana_manager.get_team_id(request.user.project_id)

        is_user_in_team = \
            grafana_manager.is_user_in_team(request.user.id, team_id) \
            if team_id is not None \
            else False
        is_user_in_org =\
            grafana_manager.is_user_in_org(request.user.id, organization_id) \
            if organization_id is not None \
            else False

        grafana_credentials = {
            "user_id": user_id,
            "organization_id": organization_id,
            "team_id": team_id,
            "is_user_in_team": is_user_in_team,
            "is_user_in_org": is_user_in_org
        }

        return grafana_credentials

    @rest_utils.ajax()
    def post(self, request):

        # def create_grafana_os_user(self, login, name, password, org, email=None):
        #def create_grafana_os_structure(self, user_login, user_name, password, org, keystone_url, email=None, domain='default'):

        '''
        ret = GrafanaManager(self.api_url).create_ceilometer_user(
            request.user.id, "password", request.user.project_id
        )
        '''
        '''
        ret = GrafanaManager(self.api_url).create_grafana_os_structure(
            # login, name, password, org, email=None
            request.user.name, request.user.id, "password", request.user.project_id
        )
        '''

        ret = GrafanaManager(self.api_url).create_grafana_os_structure(
            # login, name, password, org, email=None
            request.user.username, request.user.id, "password",
            request.user.project_id, "https://sddc.safedx.eu:5000/"
        )

        # ret = None
        return ret
