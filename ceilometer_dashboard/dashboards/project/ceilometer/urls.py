from django.conf.urls import url

from ceilometer_dashboard.dashboards.project.ceilometer import views
from ceilometer_dashboard.dashboards.project.ceilometer.django_grafana_proxy import GraphanaProxyView

urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^ceilometer/(?P<path>.*)$', GraphanaProxyView.as_view(), name='proxy')
]
