from django.views import generic


class IndexView(generic.TemplateView):
    template_name = 'project/ceilometer/index.html'
