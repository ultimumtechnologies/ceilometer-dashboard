from django.utils.translation import ugettext_lazy as _
import horizon


class CeilometerPanel(horizon.Panel):
    name = _("Ceilometer")
    slug = "ceilometer"