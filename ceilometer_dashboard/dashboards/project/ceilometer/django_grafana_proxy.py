"""
## Install the Django reverse proxy package: `pip install django-revproxy`

## Enable auth.proxy authentication in Grafana like the following

```
[auth.proxy]
enabled = true
header_name = X-WEBAUTH-USER
header_property = username
auto_sign_up = true
ldap_sync_ttl = 60
whitelist = 127.0.0.1
```

The whitelist parameter can be set if Django and Grafana are in the same host.

After that add this line in the Django project `urls.py`:

```
url(r'^dashboard/(?P<path>.*)$', views.GraphanaProxyView.as_view(), name='graphana-dashboards')
```

"""
from revproxy.views import ProxyView
from django.conf import settings

try:
    GRAFANA_HOST = settings.GRAFANA_HOST
except AttributeError:
    GRAFANA_HOST = 'http://localhost:3000'


class GraphanaProxyView(ProxyView):
    # todo: softcode upstream url
    upstream = GRAFANA_HOST

    def get_proxy_request_headers(self, request):
        headers = super(GraphanaProxyView, self).get_proxy_request_headers(
            request
        )
        # todo: temporary grafana admin access
        if request.user.username == 'admin' and request.user.project_name == 'admin':
            headers['X-WEBAUTH-USER'] = 'admin'
        else:
            headers['X-WEBAUTH-USER'] = request.user.id
        #print('X-WEBAUTH-USER: ' + headers['X-WEBAUTH-USER'])

        #this.default_headers['X-Auth-Token'] = result.headers('X-Subject-Token');
        # self.request.session['token'] = self.token.id

        '''
        headers['X-Auth-Token'] = request.session['token'].id

        print(request.session['token'].id)
        '''

        return headers
