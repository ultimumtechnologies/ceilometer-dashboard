(function(){
  'use strict';

  angular
    .module('horizon.dashboard.project.ceilometer-dashboard')
    .component('ceilometer', {
      templateUrl: getTemplateUrl,
      controller: grafanaController,
      bindings: {}
    });

  grafanaController.$inject = [
    'horizon.framework.util.http.service',
    '$window'
  ];

  function grafanaController(apiService, $window) {
    var ctrl = this;

    ctrl.$onInit = function() {
      ctrl._loaded = false;
      ctrl._data = {
        user_id: 'not loaded',
        team_id: 'not loaded',
        organization_id: 'not loaded',
        is_user_in_team: 'not loaded',
        is_user_in_org: 'not loaded'
      }
    };

    ctrl._loadGrafanaUser = function() {
     return apiService
        .get('/api/ceilometer/users')
        .success(function(data) {
          ctrl._data = angular.extend({}, data);
          ctrl._data.is_user_in_team = !!data.is_user_in_team;
          ctrl._data.is_user_in_org = !!data.is_user_in_org;
          ctrl._loaded = true
        })
        .error(function(reason) {
          // todo: toast
          console.log('WIP: error loading user data', reason)
        });
    };

    ctrl.getGrafanaUser = function() {
      ctrl._loadGrafanaUser().success(
        () => {
          console.log('WIP: User data loaded successfully')
        }
      );
    };

    ctrl._createGrafanaUser = function( ){
      return apiService
        .post('/api/ceilometer/users/')
        .success(function(data) {
          // todo: something to do here?
        })
        .error(function(reason) {
          // todo: toast
          console.log('WIP: error creating user', reason)
        });
    };

    ctrl.createGrafanaUser = function () {
      ctrl._createGrafanaUser().success(() => {
        console.log('WIP: User created successfully');
      })
    };

    ctrl.step1 = function () {
      if(!ctrl.loaded) {
        ctrl._loadGrafanaUser().success(() => {
          console.log('Grafana user created; re-starting redirect');
          ctrl.step2();
        });
      } else {
        ctrl.step2();
      }
    };

    ctrl.step2 = function() {
      if(!ctrl._data.is_user_in_team || !ctrl._data.is_user_in_org) {
        ctrl._createGrafanaUser().success(() => {
          console.log('Grafana user created; re-starting redirect');
          ctrl.step3();
        });
      } else {
        ctrl.step3();
      }
    };

    ctrl.step3 = function() {
      $window.location.href = '/project/ceilometer/grafana/';
    };

    // todo: a cheeky recursion based prototype
    ctrl.redirectToGrafana = function () {
      /*
      if(!ctrl.loaded) {
        ctrl._loadGrafanaUser().success(() => {
            console.log('Grafana user created; re-starting redirect');
            //ctrl.redirectToGrafana();
        });
      }

      if(ctrl._data.is_user_in_team || ctrl._data.is_user_in_org) {
        ctrl._createGrafanaUser().success(() => {
          console.log('Grafana user created; re-starting redirect');
          //ctrl.redirectToGrafana();
        });
        return;
      }
      */

      //$window.location.href = '/project/grafana/grafana/';
      ctrl.step1();
    };
  }

  getTemplateUrl.$inject = ['horizon.dashboard.project.ceilometer.basePath'];

  function getTemplateUrl(basePath) {
    return basePath + "components/ceilometer.html";
  }
})();