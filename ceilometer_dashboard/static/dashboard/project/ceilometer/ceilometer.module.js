(function() {
  'use strict';

  angular
    .module('horizon.dashboard.project.ceilometer-dashboard', [
      'horizon.framework.conf',
      'horizon.framework.widgets',
      'horizon.framework.util',
      'horizon.app.core'
    ], config);


  config.$inject = ['$provide', '$windowProvider'];

  function config($provide, $windowProvider) {
    var basePath = $windowProvider.$get().STATIC_URL + 'dashboard/project/ceilometer/';
    $provide.constant('horizon.dashboard.project.ceilometer.basePath', basePath);
  }

})();
